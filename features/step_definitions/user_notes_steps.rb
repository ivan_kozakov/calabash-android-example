require 'calabash-android/calabash_steps'

Given(/^My app is open$/) do
  wait_for_elements_exist("* marked:'#{BasePage::ADD_NOTE}'")
end

When(/^I can tap "(.*?)" text$/) do |text|
  tap_mark(text)
end

Then(/^Select "(.*?)" note type$/) do |note_type|
  tap_mark(note_type)
end

Then(/^I can type the note name "(.*?)"$/) do |name|
  @b_p.pause 2
  @note_name = name
  keyboard_enter_text(name)
end

Then /^I can go back$/ do
  2.times do
    @b_p.go_back
    @b_p.pause
  end
end

And(/^I see my note saved$/) do
  check_element_exists("* marked:'#{@note_name}'")
end

Then(/^I can tap on "(.*?)"$/) do |name|
  tap_mark(name)
end

Then(/^I can tap the edit icon$/) do
  wait_for_element_exists("* id:'edit_btn'")
  touch("* id:'edit_btn'")
end

And(/^I remove the old text$/) do
  clear_text
end

And(/^Type "(.*?)" description$/) do |new_name|
  @b_p.pause(2)
  keyboard_enter_text(new_name)
end

And(/^I see "(.*?)" note saved$/) do |new_name|
   check_element_exists("* marked:'#{new_name}'")
end


