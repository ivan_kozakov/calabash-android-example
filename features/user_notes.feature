Feature: User Notes

  This feature should allow to create/delete/edit notes.

  Scenario: Note creation
    When My app is open
    Then I can tap "Add Note" text
    And Select "Text" note type
    Then I can type the note name "My First Note"
    Then I can go back
    And I see my note saved

  Scenario: Note editing
    Then I can tap on "My First Note"
    Then I can tap the edit icon
    And I remove the old text
    And Type "My First Changed Note" description
    Then I can go back
    And I see "My First Changed Note" note saved



