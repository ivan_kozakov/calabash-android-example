require 'forwardable'

class BasePage

  # Some constalns can go here
  ADD_NOTE = 'Add Note'

  extend Forwardable

  attr_accessor :ctx

  METHODS = [ :press_back_button ]

  def_delegators :@ctx, *METHODS

  def initialize(ctx)
    self.ctx = ctx
  end

  def go_back
    press_back_button
  end

  def pause(s=1)
    sleep s
  end
end