class NotesListPage < BasePage

  def initialize(ctx)
    self.ctx = ctx
  end

  def notes_list ; end

  def open_note(note_name)
    tap_mark note_name
  end
end