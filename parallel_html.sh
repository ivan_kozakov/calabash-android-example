#!/usr/bin/env bash

DEVICE_1=emulator-5554
DEVICE_2=emulator-5556
DEVICE_3=emulator-5558

APK_FILE=apk/example_app.apk

USER=$(whoami)
PATH_TO_LOG=/home/${USER}/projects/explorer/public/calabash_reports

screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_1} && calabash-android run ${APK_FILE} --format html --out ${PATH_TO_LOG}/${DEVICE_1}.html"
screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_2} && calabash-android run ${APK_FILE} --format html --out ${PATH_TO_LOG}/${DEVICE_2}.html"
screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_3} && calabash-android run ${APK_FILE} --format html --out ${PATH_TO_LOG}/${DEVICE_3}.html"
