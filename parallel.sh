#!/usr/bin/env bash

DEVICE_1=emulator-5554
DEVICE_2=emulator-5556
DEVICE_3=emulator-5558
DEVICE_4=emulator-5560

APK_FILE=apk/example_app.apk

screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_1} && calabash-android run ${APK_FILE} > logs/${DEVICE_1}.log"
screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_2} && calabash-android run ${APK_FILE} > logs/${DEVICE_2}.log"
screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_3} && calabash-android run ${APK_FILE} > logs/${DEVICE_3}.log"
screen -dm -t screen1 bash -c "export ADB_DEVICE_ARG=${DEVICE_4} && calabash-android run ${APK_FILE} > logs/${DEVICE_4}.log"